#include "WriteToWebserver.h"
#include <errno.h>
#include <iostream>
#include <fstream>

	WriteToWebserver::WriteToWebserver()
	{
	}

	void WriteToWebserver::writeString(const std::string& line, const std::string& fileName) const
	{
		//Write data to file that is served by Apache
		std::ofstream outputFile;
		
		try {
			outputFile.open (fileName, std::ofstream::out | std::ofstream::trunc);
		}
		catch (std::ios_base::failure& e) {
			std::cerr << "Error while opening file:" << '\n';
			std::cerr << e.what() << '\n';
		}
		
		
		//outputFile.open (fileName);
		if (outputFile.is_open()) 
		{
			outputFile << line << std::endl;
		}
		else
		{
			std::cerr << "Unable to open output file, " << fileName << ", errno= " << errno << std::endl;				
		}
		outputFile.close();
	}	

