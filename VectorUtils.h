#pragma once

#include "Vector.h"
#include "Matrix3d.h"
#include "Rotation.h"
#include <string>
#include <vector>

class VectorUtils
{
	public:
		static const int MAX_N = 10;
		VectorUtils();
		double magnitude(const Vector& p) const;
		std::string toString(const Vector& p) const;
		std::string toString(const Rotation& r) const;		
		Vector normalize(const Vector& p) const;	
		Vector crossProduct(const Vector& a, const Vector& b) const;	
		Vector add(const Vector& a, const Vector& b) const;
		double dotProduct(const Vector& a, const Vector& b) const;
		double angleBetweenVectors(const Vector& a, const Vector& b) const;
		Matrix3d SkewSymetricCrossProduct(const Vector& v) const;	
		Matrix3d identityMatrix3d() const;
		Vector scalarMultiply(const Vector& p, double c) const;
		Rotation getRotationParameters(const Vector& v, const Vector& k) const;
		Vector rotate(const Rotation& r, const Vector& v) const;
		double magneticDeclination(const Vector& v) const;
		double magneticInclination(const Vector& v) const;
		double magneticBearing(const Vector& v) const;
		std::vector<std::vector<double>> MatrixMultiply(std::vector<std::vector<double>> a, std::vector<std::vector<double>> b);
	private:
};
