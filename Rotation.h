#pragma once
#include <string>
#include "Vector.h"

struct Rotation
{
private:

public:
		struct Vector axisOfRotation;
		double theta;

		friend std::ostream& operator<<(std::ostream &Str, Rotation const &r);
};

 
