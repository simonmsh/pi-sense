#pragma once

#include <string>
#include <vector>


class MatrixUtils
{
	public:
		static const int MAX_N = 10;
		MatrixUtils();
		std::string toString(std::vector<double> a) const;
		std::string toString(std::vector<std::vector<double>> a) const;
		void LUDecomposition(std::vector<std::vector<double>> a, std::vector<std::vector<double>> &l, std::vector<std::vector<double>> &u);
		double LUDetermintant(std::vector<std::vector<double>> l, std::vector<std::vector<double>> u);
		std::vector<std::vector<double>> MatrixMultiply(std::vector<std::vector<double>> a, std::vector<std::vector<double>> b);
		std::vector<std::vector<double>> Gauss(std::vector<std::vector<double>> A);
		std::vector<std::vector<double>> Inverse(std::vector<std::vector<double>> A);
		std::vector<std::vector<double>> Transpose(std::vector<std::vector<double>> A); 
	private:
};
