#include "VectorUtils.h"
#include <iostream>
#include <cmath>
#include <string>
#include <vector>

	VectorUtils::VectorUtils()
	{
	}

	double pi = 3.1428;
	double d180 = 180.0;

	// Return the dot product of 2 vectors
	double VectorUtils::dotProduct(const Vector& a, const Vector& b) const
	{
		return a.x * b.x + a.y * b.y + a.z * b.z; 
	}

	// Return the angle between 2 vectors in radians
	double VectorUtils::angleBetweenVectors(const Vector& a, const Vector& b) const
	{
		return std::acos(VectorUtils::dotProduct(a,b) / (VectorUtils::magnitude(a)*VectorUtils::magnitude(b)));
	}

	//Rotate vector v about the unit vector k through angle theta
	//Vector VectorUtils::rotateVectorAboutVector(const Vector& v, const Vector& k, const double theta) const
	//{
	//	struct Vector v;
	//	return v;
	//}
	
	
	Matrix3d VectorUtils::SkewSymetricCrossProduct(const Vector& v) const
	{
		struct Matrix3d crossProduct;
		crossProduct.t11 = 0.0;  crossProduct.t12 = -v.z; crossProduct.t13 = v.y;
		crossProduct.t21 = v.z;  crossProduct.t22 = 0.0;  crossProduct.t23 = -v.x;
		crossProduct.t31 = -v.y; crossProduct.t32 = v.x;  crossProduct.t33 = 0.0;
		return crossProduct;
	}

	// Return a 3x3 identity matrix
	Matrix3d VectorUtils::identityMatrix3d() const
	{
		struct Matrix3d identity;
		
		identity.t11=1.0; identity.t12=0.0; identity.t13=0.0; 
		identity.t21=0.0; identity.t22=1.0; identity.t23=0.0; 
		identity.t31=0.0; identity.t32=0.0; identity.t33=1.0; 
		
		return identity;
	}

	// Return the cross product of 2 vectors
	Vector VectorUtils::crossProduct(const Vector& a, const Vector& b) const
	{
		struct Vector v;
		v.x = a.y*b.z - a.z*b.y;
		v.y = a.z*b.x - a.x*b.z;
		v.z = a.x*b.y - a.y*b.x;
		v.W = 1.0;
		
		return v;
	}

	//Given a vector representing the local magnetic field, return the declination (degrees that magnetic north is east or west of geographic north)
	double VectorUtils::magneticDeclination(const Vector& v) const 
	{
		return d180 * std::atan(v.y/v.x) / pi;
	}


	//Given a vector representing the local magnetic field, return the inclination (degrees above or below the Earth's surface that field is pointing)
	double VectorUtils::magneticInclination(const Vector& v) const 
	{
		double surfaceComponent; //Magnitude of magnetic field vector lying on Earth's surface
		surfaceComponent = std::sqrt(v.x*v.x + v.y*v.y);
		return d180 * std::atan(v.z / surfaceComponent) / pi;
	}


	//Given a vector representing the local magnetic field, return the magnetic bearing in degrees (0 = north, 90 = east)
	double VectorUtils::magneticBearing(const Vector& v) const 
	{
		double theta;
		
		if (v.x < 0.0 && v.y > 0.0) {
			//First quadrant
			theta = std::atan(-v.y / v.x);
		}
		if (v.x > 0.0 && v.y > 0.0) {
			//Second quadrant
			theta = pi - std::atan(v.y / v.x);
		}
		if (v.x > 0.0 && v.y < 0.0) {
			//Third quadrant
			theta = pi + std::atan(v.y / -v.x);
		}
		if (v.x < 0.0 && v.y < 0.0) {
			//Fourth quadrant
			theta = 2.0 * pi - std::atan(v.y / v.x);
		}
		return d180 * theta / pi;
	}

	// Add 2 vectors
	Vector VectorUtils::add(const Vector& a, const Vector& b) const
	{
		struct Vector v;
		v.x = a.x + b.x;
		v.y = a.y + b.y;
		v.z = a.z + b.z;
		v.W = 1.0;
		
		return v;
	}

	// Return the magnitude of a vector
	double VectorUtils::magnitude(const Vector& p) const
	{
		return std::sqrt(p.x * p.x + p.y * p.y + p.z * p.z);
	}
	
	
	// Normalise a vector, i.e. return a unit vector
	Vector VectorUtils::normalize(const Vector& p) const
	{
		struct Vector newP;
		double magnitude = VectorUtils::magnitude(p);
		newP.x = p.x / magnitude;
		newP.y = p.y / magnitude;
		newP.z = p.z / magnitude;
		newP.W = 1.0;
		return newP;
	}

	// Perform scalar multiplication of the supplied vector
	Vector VectorUtils::scalarMultiply(const Vector& p, double c) const
	{
		struct Vector newP;
		newP.x = p.x * c;
		newP.y = p.y * c;
		newP.z = p.z * c;
		newP.W = 1.0;
		return newP;
	}

	//Rotate the specified vector using the supplied rotation parameters
	Vector VectorUtils::rotate(const Rotation& r, const Vector& v) const
	{
		struct Vector t1 = VectorUtils::scalarMultiply(v, std::cos(r.theta));

		struct Vector t2 = VectorUtils::crossProduct(r.axisOfRotation,v);
		t2 = VectorUtils::scalarMultiply(t2, std::sin(r.theta));

		struct Vector t3 = VectorUtils::scalarMultiply(r.axisOfRotation, VectorUtils::dotProduct(r.axisOfRotation,v));
		t3 = VectorUtils::scalarMultiply(t3, (1-std::cos(r.theta)));

		struct Vector vrot = VectorUtils::add(t1, t2);
		vrot = VectorUtils::add(vrot,t3);
		
		return vrot;
	}

	//Return the parameters needed to rotate vector v to unit vector target, using Rodrigues' formula
	Rotation VectorUtils::getRotationParameters(const Vector& v, const Vector& target) const
	{
		struct Rotation rotation;

		struct Vector k = VectorUtils::crossProduct(v, target);
		k = VectorUtils::normalize(k);
		
		double theta = VectorUtils::angleBetweenVectors(v,target);

		rotation.axisOfRotation = k;
		rotation.theta = theta;
		
		return rotation;
	}

	
	// Return a string representation of the supplied vector
	std::string VectorUtils::toString(const Vector& p) const
	{
		std::string answer;
		
		answer = "x= " +	std::to_string(p.x) + ", y= " + std::to_string(p.y) + ", z= " + std::to_string(p.z) + ", W= " + std::to_string(p.W) + ", mag=" + std::to_string(VectorUtils::magnitude(p));
		return answer;
	}	

	// Return a string representation of the supplied rotation (i.e. a combination of a unit vector specifying the axis of rotation and a rotation angle in radians)
	std::string VectorUtils::toString(const Rotation& r) const
	{
		std::string answer;
		
		answer = "x=" +	std::to_string(r.axisOfRotation.x) + ", y=" + std::to_string(r.axisOfRotation.y) + ", z=" + std::to_string(r.axisOfRotation.z) + ", W=" + std::to_string(r.axisOfRotation.W) + ", mag=" + std::to_string(VectorUtils::magnitude(r.axisOfRotation)) + ", theta=" + std::to_string(r.theta);
		return answer;
	}	

