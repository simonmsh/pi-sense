#include <thread>
#include <ostream>
#include <cmath>
#include <iostream>
#include "WriteToWebserver.h"
#include "VectorUtils.h"
#include "IMUInterface.h"
#include "RTIMULib.h"
#include "PressureHistory.h"
	IMUInterface::IMUInterface()
	{
		
	}
	
	
	// Initialise a connection to IMU hardware, and begin collecting data from it at periodic intervals
	void IMUInterface::init ()
	{
		std::cout << "IMU thread starting" <<  std::endl;

		int sampleCount = 0;
		uint64_t displayTimer;
		uint64_t now;
		double xSumA=0.0, ySumA=0.0, zSumA=0.0;
		double xSumM=0.0, ySumM=0.0, zSumM=0.0;
		double alpha = 0.05;
		double sumAccel=0.0, sumAccelSquared=0.0, minAccel=10.0, maxAccel=0.0, meanAccel;
		const std::string accFilename = "//var//www//html//acc.xml";
		bool justStarted = true;
    
		auto webserverWriter = std::make_unique<WriteToWebserver>();
		//auto pressureHistory = std::make_unique<PressureHistory>();
		pressureHistory = new PressureHistory();
		pressureHistory->Init(100);
		std::array<double, MAX_SAMPLES> accelMagnitudes;
		RTIMUSettings *settings = new RTIMUSettings("RTIMULib");
		RTIMU *imu = RTIMU::createIMU(settings);
		RTPressure *pressure = RTPressure::createPressure(settings); 
		RTHumidity *humidity = RTHumidity::createHumidity(settings);
		if ((imu == NULL) || (imu->IMUType() == RTIMU_TYPE_NULL)) {
			std::cout << "No IMU found\n" << std::endl;
			exit(1);
		}

		//  This is an opportunity to manually override any settings before the call IMUInit
		imu->IMUInit();
		if (pressure != NULL) pressure->pressureInit();
		if (humidity != NULL) humidity->humidityInit();    

		//  this is a convenient place to change fusion parameters
		imu->setSlerpPower(0.02);
		imu->setAccelEnable(true);
		imu->setCompassEnable(true);

		auto vectorUtils = std::make_unique<VectorUtils>();
		std::cout << "Poll interval=" << std::to_string(imu->IMUGetPollInterval()) << "ms" << std::endl;			
		std::cout << "DEVIATION_THRESHOLD=" << std::to_string(DEVIATION_THRESHOLD) << "g" << std::endl;			
		
		for (int i=0; i < MAX_SAMPLES; i++) {
			accelMagnitudes[i] = 0.0;
			accelMagnitudesSnapshot[i] = 0.0;
		}
		while (running) {
			//  poll at the rate recommended by the IMU
			usleep(imu->IMUGetPollInterval() * 1000);
			while (imu->IMURead()) {
				RTIMU_DATA imuData = imu->getIMUData();
				xSumA += imuData.accel.x(); ySumA += imuData.accel.y(); zSumA += imuData.accel.z();
				xSumM += imuData.compass.x(); ySumM += imuData.compass.y(); zSumM += imuData.compass.z();
				struct Vector accelSample = {imuData.accel.x(), imuData.accel.y(), imuData.accel.z(), 1.0};
				double accelMagnitude = vectorUtils->magnitude(accelSample);
				if (accelMagnitude < 0.97 || accelMagnitude > 1.03) {
					std::cout 
					<< "x=" << std::to_string(imuData.accel.x()) 
					<< " y=" << std::to_string(imuData.accel.y()) 
					<< " z=" << std::to_string(imuData.accel.z()) 
					<< " mag=" << std::to_string(accelMagnitude) 
					<< std::endl;					
				}
				sumAccel += accelMagnitude;
				sumAccelSquared += accelMagnitude * accelMagnitude;
				if (sampleCount < MAX_SAMPLES) accelMagnitudes[sampleCount] = accelMagnitude;
				sampleCount++;
				if (accelMagnitude < minAccel) minAccel = accelMagnitude;
				if (accelMagnitude > maxAccel) maxAccel = accelMagnitude;

				now = RTMath::currentUSecsSinceEpoch();

				//  display 1 times per second
				if ((now - displayTimer) > 2000000) {
					std::time_t timeNow = std::time(0);   // get time now
					std::tm* tmNow = std::localtime(&timeNow);

					if (pressure != NULL) pressure->pressureRead(imuData);
					if (humidity != NULL) humidity->humidityRead(imuData);				
					
					xSumA = xSumA / sampleCount; ySumA = ySumA / sampleCount; zSumA = zSumA / sampleCount;
					xSumM = xSumM / sampleCount; ySumM = ySumM / sampleCount; zSumM = zSumM / sampleCount;

					struct Vector a = {xSumA, ySumA, zSumA, 1.0};
					struct Vector m = {xSumM, ySumM, zSumM, 1.0};

					// Use gravitational vector to correct magnetic vector for sensor being rolled or pitched WRT to horizontal. Yaw cannot be corrected for.
					struct Vector tEC = {0.0, 0.0, 1.0, 1.0};  //Assume negative z-axis points towards centre of Earth.
					tEC = vectorUtils->normalize(tEC);
					
					struct Rotation rotation = vectorUtils->getRotationParameters(a, tEC);
					accel = vectorUtils->rotate(rotation, a);
					mag = vectorUtils->rotate(rotation, m);
					if (justStarted) {
						std::cout << "rotation= " << rotation << std::endl;					
						justStarted = false;
					}

					sampleFrequency = sampleCount / ((now - displayTimer) /1000000);

					if (sampleCount > 1) accelStandardDeviation = std::sqrt((sumAccelSquared - sumAccel*sumAccel/sampleCount) / (sampleCount - 1) );
					if (accelStandardDeviation > maxAccelStandardDeviation) maxAccelStandardDeviation = accelStandardDeviation;
					meanAccel = sumAccel/sampleCount;
					if (maxAccel - meanAccel > meanAccel - minAccel) {
						if (accelGreatestDeviation <  maxAccel - meanAccel) accelGreatestDeviation = maxAccel - meanAccel;
					} else {
						if (accelGreatestDeviation < meanAccel - minAccel) accelGreatestDeviation = meanAccel - minAccel;
					}
					if (maxAccel - meanAccel > DEVIATION_THRESHOLD || meanAccel - minAccel > DEVIATION_THRESHOLD) {
						std::string message = "---  " + std::to_string(tmNow->tm_hour) + ':' + std::to_string(tmNow->tm_min) + ':' +  std::to_string(tmNow->tm_sec) + 
							" MAX=" + std::to_string(maxAccel - meanAccel) 
							+ " MIN=" + std::to_string(meanAccel - minAccel)
							+ " MEAN=" + std::to_string(meanAccel)
							+ "\n"; 
							std::cout << message;
							webserverWriter->writeString(message, accFilename);
							for (int i=0; i < MAX_SAMPLES; i++) accelMagnitudesSnapshot[i] = accelMagnitudes[i];
					}
					minAccel=10.0;
					maxAccel=0.0;
					for (int i=0; i < MAX_SAMPLES; i++) accelMagnitudes[i] = 0.0;

					if (smoothedHumidity!=0.0)
						smoothedHumidity = smoothedHumidity * (1.0 - alpha) + imuData.humidity * alpha;
					else
						smoothedHumidity = imuData.humidity;
					smoothedPressure = imuData.pressure;
					smoothedTemperature = imuData.temperature;
					pressureHistory->update(std::make_pair(timeNow,imuData.pressure));

					displayTimer = now;
					sampleCount = 0;
					sumAccel = 0.0;	
					sumAccelSquared = 0.0;					
					xSumA = 0.0; ySumA = 0.0; zSumA = 0.0;
					xSumM = 0.0; ySumM = 0.0; zSumM = 0.0;
				}
			}
		}
		std::cout << "IMU thread terminating" <<  std::endl;
	}		
	
	double IMUInterface::getHumidity() const
	{
		return smoothedHumidity;
	}

	void IMUInterface::terminate()
	{
		std::cout << "IMUInterface::terminate() called" << std::endl;
		running =false;
	}

	double IMUInterface::getPressure() const
	{
		return smoothedPressure;
	}

	double IMUInterface::getTemperature() const
	{
		return smoothedTemperature;
	}

	double IMUInterface::getAccelStandardDeviation() const
	{
		return accelStandardDeviation;
	}

	double IMUInterface::getSampleFrequency() const
	{
		return sampleFrequency;
	}

	double IMUInterface::getMaxAccelStandardDeviation()
	{
		double answer = maxAccelStandardDeviation;
		maxAccelStandardDeviation = 0.0;
		return answer;
	}

	// Return an array of acceleration magnitudes from the last averaging period. Useful for examining mechanical shocks.
	std::array <double, IMUInterface::MAX_SAMPLES> IMUInterface::getAccelMagnitudesSnapshot()
	{
		std::array <double, IMUInterface::MAX_SAMPLES> temp;
		for (int i=0; i < MAX_SAMPLES; i++) {
			temp[i] = accelMagnitudesSnapshot[i];
			accelMagnitudesSnapshot[i] = 0.0;
		}
		return temp;
	}

	// Return the largest deviation from mean acceleration for the averaging period. Units are standard g. 
	double IMUInterface::getAccelGreatestDeviation()
	{
		double answer = accelGreatestDeviation;
		accelGreatestDeviation = 0.0;
		return answer;
	}

	// Get the pressure gradient and half confidence interval for the last 3 hours. Units are millibars per hour.
	std::pair<double, double> IMUInterface::getPressureGradient()
	{
		return pressureHistory->PressureChangePerHour();
	}
	
	// Return the instantaneous acceleration vector. Units are standard g (9.80665 m/s^2)
	Vector IMUInterface::getAccel() const
	{
		return accel;
	}

	// Return the instantaneous magnetic field vector. Units are microTesla
	Vector IMUInterface::getMag() const
	{
		return mag;
	}

