#include <iostream>
#include "Rotation.h"

std::ostream& operator<<(std::ostream &Str, Rotation const &r) { 
  // print something from v to str, e.g: Str << v.getX();
  Str << "theta=" << r.theta << " radians " << r.theta * 180 / 3.1428 << " degrees" ; 
  return Str;
}

