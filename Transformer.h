#pragma once

#include <string>
#include "Vector.h"
#include "Transformation.h"


class Transformer
{
	public:
		Transformer();
		Transformation compose(const Transformation& oldT, const Transformation& t) const;
		std::string toString(const Transformation& t) const;
		Transformation identity() const;
		Transformation translation(const double dx, const double dy, const double dz) const;
		Transformation scaling(const double sx, const double sy, const double sz) const;
		Transformation xAxisRotation(const double theta) const;
		Transformation yAxisRotation(const double theta) const;
		Transformation zAxisRotation(const double theta) const;	
		Transformation unitVectorRotation(const double theta, const Vector& u) const;
		Vector transform(const Vector& p, const Transformation& t) const;
	private:
};
