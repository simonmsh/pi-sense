#pragma once

struct TimestampedFloat
{
		time_t ts;
		double value;
};
