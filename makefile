#This sample makefile has been setup for a project which contains the following files: main.h, ap-main.c, ap-main.h, ap-gen.c, ap-gen.h   Edit as necessary for your project

#Change output_file_name.a below to your desired executible filename

#Set all your object files (the object files of all the .c files in your project, e.g. main.o my_sub_functions.o )
OBJ = Pi-sense.o VectorUtils.o Vector.o Transformation.o Transformer.o Matrix3d.o Rotation.o WriteToWebserver.o IMUInterface.o TimestampedFloat.o PressureHistory.o MatrixUtils.o

#Set any dependant header files so that if they are edited they cause a complete re-compile (e.g. main.h some_subfunctions.h some_definitions_file.h ), or leave blank
DEPS = VectorUtils.h Vector.h Transformation.h Transformer.h Matrix3d.h Rotation.h WriteToWebserver.h IMUInterface.h TimestampedFloat.h PressureHistory.h MatrixUtils.h

#Any special libraries you are using in your project (e.g. -lbcm2835 -lrt `pkg-config --libs gtk+-3.0` ), or leave blank
LIBS = -lRTIMULib 
#-lbcm2835 -lrt

#Set any compiler flags you want to use (e.g. -I/usr/include/somefolder `pkg-config --cflags gtk+-3.0` ), or leave blank
CFLAGS = -std=c++17 -Wall -pedantic -pthread -Wno-psabi

#Set the compiler you are using ( gcc for C or g++ for C++ )
CC = /opt/gcc-9.1.0/bin/g++-9.1

#Get the filename extensiton of your C files (e.g. .c or .cpp )
EXTENSION = .cpp

#define a rule that applies to all files ending in the .o suffix, which says that the .o file depends upon the .c version of the file and all the .h files included in the DEPS macro.  Compile each object file
%.o: %$(EXTENSION) $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

#define a rule that applies to all files ending in the .o suffix, which says that the .o file depends upon the .c version of the file and all the .h files included in the DEPS macro.  Compile each object file
#%.o: %.cpp $(DEPS)
#	$(CC) -c -o $@ $< $(CFLAGS)


#Combine them into the output file
#Set your desired exe output file name here
Pi-sense: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

#Cleanup
.PHONY: clean

clean:
	rm -f *.o *~ core *~ 
