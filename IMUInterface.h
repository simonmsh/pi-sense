#pragma once
#include <string>
#include <iostream>
#include "Vector.h"
#include "PressureHistory.h"

class IMUInterface
{
	private:
		static const int MAX_SAMPLES = 100;
		double smoothedHumidity=0.0, smoothedPressure=0.0, smoothedTemperature=0.0, accelStandardDeviation=0.0, maxAccelStandardDeviation=0.0;
		double accelGreatestDeviation=0.0, sampleFrequency=0.0;
		double const DEVIATION_THRESHOLD = 0.0042;
		struct Vector accel = {0.0, 0.0, 0.0, 1.0};
		struct Vector mag = {0.0, 0.0, 0.0, 1.0};
		bool running=true;	
		std::array<double, MAX_SAMPLES> accelMagnitudesSnapshot;
		PressureHistory * pressureHistory;

	public:
		IMUInterface();
		void init() ;
		double getHumidity() const;
		double getTemperature() const;
		double getPressure() const;
		double getAccelStandardDeviation() const;
		double getMaxAccelStandardDeviation();
		std::array <double, MAX_SAMPLES> getAccelMagnitudesSnapshot();		
		double getAccelGreatestDeviation();
		double getSampleFrequency() const;
		std::pair<double, double> getPressureGradient();
		Vector getAccel() const;
		Vector getMag() const;
		void terminate();

};
