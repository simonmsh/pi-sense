#pragma once

#include <string>
#include <queue>
#include <tuple>
#include <thread>
#include <mutex>  

class PressureHistory
{
	public:
		PressureHistory();
		std::string toString() const;
		void Init(int size);
		void update(std::pair<time_t, double> value);
		std::pair<double, double> PressureChangePerHour(); 		
	private:
		std::deque < std::pair<time_t, double> > history;
		unsigned int maxSize;
		int const TIMESPAN = 3 * 3600;
		std::mutex mtx;
};
