#include "MatrixUtils.h"
#include <iostream>
#include <cmath>
#include <string>
#include <vector>

	MatrixUtils::MatrixUtils()
	{
	}


	std::string MatrixUtils::toString(std::vector<std::vector<double>> a) const
	{
		std::string answer;

		answer = "";
		for (unsigned int i=0; i < a.size(); i++)  // i is row index
		{
			for (unsigned int j=0; j < a[0].size(); j++) // j is column index
			{
				answer+= std::to_string(a[i][j]) + ",";
			}
			answer += "\n";
		}

		
		return answer;
	}	


	std::string MatrixUtils::toString(std::vector<double> a) const
	{
		std::string answer;

		answer = "";
		for (unsigned int i=0; i < a.size(); i++)  // i is row index
		{
			answer+= std::to_string(a[i]) + ",";
		}

		
		return answer;
	}	


	// Multiply b by a and return the result
	std::vector<std::vector<double>> MatrixUtils::MatrixMultiply(std::vector<std::vector<double>> a, std::vector<std::vector<double>> b) {
		std::vector<std::vector<double>> answer(a.size(),std::vector<double> (a.size(),0.0));		
	
		for (unsigned int i=0; i < a.size(); i++)  // i is row index
		{
			for (unsigned int j=0; j < b[0].size(); j++) // j is column index
			{
				answer[i][j] = 0.0;
				for (unsigned int m=0; m < a.size(); m++)
				{
					answer[i][j] += a[i][m] * b[m][j];
				}
			}
		}
	   
		return answer;
	}


	// Return the determinant of a matrix, based on the supplied l and u representation of it
	double MatrixUtils::LUDetermintant(std::vector<std::vector<double>> l, std::vector<std::vector<double>> u) {
		double answer = 1.0;
	
		for (unsigned int i=0; i < l.size(); i++)  // i is row index
		{
			answer *= l[i][i];
			answer *= u[i][i];
			 
		}
		return answer;
	}


	std::vector<std::vector<double>> MatrixUtils::Transpose(std::vector<std::vector<double>> A) 
	{
		int sourceRows = A.size();
		int sourceColumns = A[0].size();
		std::vector<std::vector<double>> t(sourceColumns,std::vector<double> (sourceRows,0.0)) ;
		
		for (int i=0; i < sourceRows; i++)
		{
			for (int j=0; j < sourceColumns; j++) 
			{
				t[j][i] = A[i][j];
			}
		}
		return t;
	}



	std::vector<std::vector<double>> MatrixUtils::Gauss(std::vector<std::vector<double>> A) {
		int n = A.size();

		for (int i=0; i<n; i++) {
			// Search for maximum in this column
			double maxEl = abs(A[i][i]);
			int maxRow = i;
			for (int k=i+1; k<n; k++) {
				if (abs(A[k][i]) > maxEl) {
					maxEl = abs(A[k][i]);
					maxRow = k;
				}
			}

			// Swap maximum row with current row (column by column)
			for (int k=i; k<n+1;k++) {
				double tmp = A[maxRow][k];
				A[maxRow][k] = A[i][k];
				A[i][k] = tmp;
			}

			// Make all rows below this one 0 in current column
			for (int k=i+1; k<n; k++) {
				double c = -A[k][i]/A[i][i];
				for (int j=i; j<n+1; j++) {
					if (i==j) {
						A[k][j] = 0;
					} else {
						A[k][j] += c * A[i][j];
					}
				}
			}
		}

		std::cout << "A(UT)=" << std::endl << toString(A) << std::endl;

		// Solve equation Ax=b for an upper triangular matrix A
		std::vector<double> x(n);
		for (int i=n-1; i>=0; i--) {
			x[i] = A[i][n]/A[i][i];
			for (int k=i-1;k>=0; k--) {
				A[k][n] -= A[k][i] * x[i];
			}
		}
		std::cout << "A(UT2)=" << std::endl << toString(A) << std::endl;
		std::cout << "x=" << toString(x) << std::endl;

		return A;
	}

	
	//Return the inverse of A, using Gauss-Jordan algorithm
	std::vector<std::vector<double>> MatrixUtils::Inverse(std::vector<std::vector<double>> A) {
		int n = A.size();
		double d;
		int i, j, k;

		std::vector<std::vector<double>> aug(n,std::vector<double> (2*n,0.0)) ;
		for (int i=0; i < n; i++) 
		{
			for (int j=0; j < n; j++)
			{
				aug[i][j] = A[i][j];
			}
		} 
		for (int i=0; i < n; i++) 
		{
			for (int j=n; j < 2*n; j++)
			{
				if (j==i+n) aug[i][j] = 1.0;
			}
		} 
		
		// Partial pivoting
		for (i=n-1; i > 0; i--)
		{
			if (aug[i-1][1] < aug[i][1])
			{
				for (j=0; j < n*2; j++)
				{
					d = aug[i][j];
					aug[i][j] = aug[i-1][j];
					aug[i-1][j] = d;
				}
			}
		}

		// Reducing to diagonal  matrix
		for (i=0; i < n; i++)
		{
			for (j=0; j < n; j++)
			{
				if (j != i)
				{
					d = aug[j][i] / aug[i][i];
					for (k=0; k < n*2; k++) 
						aug[j][k] -= aug[i][k] * d;
				}
			}
		}

		// Reducing to unit matrix
		for (i=0; i < n; i++)
		{
			d = aug[i][i];
			for (j=0; j < n*2; j++)
				aug[i][j] = aug[i][j] / d;
		}

		for (int i=0; i < n; i++) 
		{
			for (int j=n; j < 2*n; j++)
			{
				A[i][j-n] = aug[i][j];
			}
		} 

		return A;
	}


	// Decomponse a into lower and upper triangular matrices
	void MatrixUtils::LUDecomposition(std::vector<std::vector<double>> a, std::vector<std::vector<double>> &l, std::vector<std::vector<double>> &u) {
	   unsigned int i = 0, j = 0, k = 0;
	   for (i = 0; i < a.size(); i++) {
		  for (j = 0; j < a.size(); j++) {
			 if (j < i)
				l[j][i] = 0;
			 else {
				l[j][i] = a[j][i];
				for (k = 0; k < i; k++) {
				   l[j][i] = l[j][i] - l[j][k] * u[k][i];
				}
			 }
		  }
		  for (j = 0; j < a.size(); j++) {
			 if (j < i)
				u[i][j] = 0;
			 else if (j == i)
				u[i][j] = 1;
			 else {
				u[i][j] = a[i][j] / l[i][i];
				for (k = 0; k < i; k++) {
				   u[i][j] = u[i][j] - ((l[i][k] * u[k][j]) / l[i][i]);
				}
			 }
		  }
	   }
	}

