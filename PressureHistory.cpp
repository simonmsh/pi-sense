#include "PressureHistory.h"
#include <ostream>
#include <cmath>
#include <iostream>
#include <string>
#include <chrono>
#include <limits>

	// A class used to store a list of time-stamped air pressure readings. The readings may be used to estimate the
	// recent rate of change of air pressure, and a confidence interval for that rate of change. Drops of more than
	// 6 millibars in 3 hours sometimes indicate storms in the next few hours.

	PressureHistory::PressureHistory()
	{
	}

	void PressureHistory::Init(int size)
	{
		maxSize = size;
	}

	
	// Update the pressure history queue. If the queue is not yet full, or the newest item in the queue exceeds the age threshold,
	// push the new data item onto the queue and pop the oldest existing item out of the queue.
	void PressureHistory::update(std::pair<time_t, double> value)
	{
		mtx.lock();

		//value.second = 1000.0 + (double) history.size() / 1800.0;

		//std::cout << "PUSHING size=" << std::to_string(history.size()) 
		//	<< " t=" << std::to_string(value.first)
		//	<< " p=" << std::to_string(value.second) << std::endl ;			

		if (history.size() < maxSize) 
		{
			history.push_back(value);
		}
		else
		{
			std::time_t timeNow = std::time(0);   // get time now
			time_t newest = history.back().first;
			if (((unsigned int)timeNow - newest) > TIMESPAN/history.size()) 
			{ 
				history.pop_front(); 
				history.push_back(value);
			}
		}
		mtx.unlock();
	}


	// Return a pair containing the mean rate of pressure change averaged over the last 3 hours, and half the 95% confidence interval.
	// Units are millibars per hour.
	std::pair<double, double> PressureHistory::PressureChangePerHour() 
	{
		mtx.lock();

		double m=0.0, c=0.0;
		double sumX=0.0, sumXX=0.0, sumY=0.0, sumXY=0.0, sumSquaredErrors=0.0, sumSquaredXDifferences=0.0;
		double meanX=0.0, meanY=0.0, standardErrorM, halfGradientConfidenceInterval=0.0;
		double minP=10000.0, maxP=0.0, minT=std::numeric_limits<double>::max(), maxT=0.0;
		double sxx, sxy;
		if (history.size() > 2) {
			for (unsigned int i=0; i < history.size(); i++) 
			{
				//std::cout << "history[" << std::to_string(i) << "].first=" << history[i].first << std::endl;
				double xInHours = (history[i].first - history[0].first) / 3600.0;
				sumX += xInHours;
				sumXY += xInHours * history[i].second;
				sumY += history[i].second;
				sumXX += xInHours * xInHours;
				if (minP > history[i].second) minP=history[i].second;
				if (maxP < history[i].second) maxP=history[i].second;
				if (minT > history[i].first) minT=xInHours;
				if (maxT < history[i].first) maxT=xInHours;
			}
			meanX = sumX / (double)history.size();
			meanY = sumY / (double)history.size();
			
			//m = (sumXY - sumX * sumY / (double)history.size()) 
			//	/ (sumXX - sumX * sumX / (double)history.size());

			sxx = sumXX - (double)history.size() * meanX * meanX;
			sxy = sumXY - (double)history.size() * meanX * meanY;
			m = sxy / sxx;
			//std::cout << "sxx=" << std::to_string(sxx) << " sxy=" << std::to_string(sxy) << " m=" << std::to_string(m) << std::endl; 
			
			c = meanY - m * meanX;

			for (unsigned int i=0; i < history.size(); i++) 
			{
				double xInHours = history[i].first / 3600.0;
				sumSquaredErrors += std::pow(history[i].second - c - m * xInHours, 2);
				sumSquaredXDifferences += std::pow(xInHours - meanX, 2);
			}
			standardErrorM = std::sqrt(
						((1.0/((double) history.size()-2.0)) * sumSquaredErrors) 
						/ sumSquaredXDifferences
			);
			// Two-sided T-distribution, 95% confidence, infinite degrees of freedom.
			halfGradientConfidenceInterval = 1.960 * standardErrorM;

			std::cout << "size=" << std::to_string(history.size()) 
						//<< " sumX=" << std::to_string(sumX) 
						//<< " sumY=" << std::to_string(sumY) 
						//<< " sumXX=" << std::to_string(sumXX) 
						//<< " sumXY=" << std::to_string(sumXY) 
						<< " minP=" << std::to_string(minP) 
						<< " maxP=" << std::to_string(maxP) 
						//<< " meanX=" << std::to_string(meanX) 
						//<< " meanY=" << std::to_string(meanY) 
						//<< " Sxx=" << std::to_string(sxx) 
						//<< " Sxy=" << std::to_string(sxy) 
						//<< " sumSquaredErrors=" << std::to_string(sumSquaredErrors) 
						//<< " sumSquaredXDifferences=" << std::to_string(sumSquaredXDifferences) 
						<< " pSpan=" << std::to_string(maxP - minP) 
						<< " tSpan=" << std::to_string(maxT-minT) 
						<< " grad=" << std::to_string(m) 
						<< " grad+-=" << std::to_string(halfGradientConfidenceInterval) 
						<< std::endl;		
		
		}
		mtx.unlock();

		return std::make_pair(m, halfGradientConfidenceInterval);
	}

	std::string PressureHistory::toString() const
	{
		std::string answer;
		
		answer = "";
		return answer;
	}





