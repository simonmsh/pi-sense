#include "Transformer.h"
#include <ostream>
#include <cmath>

	Transformer::Transformer()
	{
	}

	std::string Transformer::toString(const Transformation& t) const
	{
		std::string answer;
		
		answer = 	std::to_string(t.t11) + "," + std::to_string(t.t12) + "," + std::to_string(t.t13) + "," + std::to_string(t.t14) + "," + "\n" +
					std::to_string(t.t21) + "," + std::to_string(t.t22) + "," + std::to_string(t.t23) + "," + std::to_string(t.t24) + "," + "\n" +
					std::to_string(t.t31) + "," + std::to_string(t.t32) + "," + std::to_string(t.t33) + "," + std::to_string(t.t34) + "," + "\n" +
					std::to_string(t.t41) + "," + std::to_string(t.t42) + "," + std::to_string(t.t43) + "," + std::to_string(t.t44) + "," + "\n";
		return answer;
	}




	Transformation Transformer::identity() const
	{
		struct Transformation newT;
		
		newT.t11=1.0; newT.t12=0.0; newT.t13=0.0; newT.t14=0.0;
		newT.t21=0.0; newT.t22=1.0; newT.t23=0.0; newT.t24=0.0;
		newT.t31=0.0; newT.t32=0.0; newT.t33=1.0; newT.t34=0.0;
		newT.t41=0.0; newT.t42=0.0; newT.t43=0.0; newT.t44=1.0;
		
		return newT;
		
	}


	Vector Transformer::transform(const Vector& p, const Transformation& t) const
	{
		struct Vector newP;
		
		newP.x = p.x * t.t11 + p.y * t.t21 + p.z * t.t31 + p.W * t.t41;
		newP.y = p.x * t.t12 + p.y * t.t22 + p.z * t.t32 + p.W * t.t42;
		newP.z = p.x * t.t13 + p.y * t.t23 + p.z * t.t33 + p.W * t.t43;
		newP.W = p.x * t.t14 + p.y * t.t24 + p.z * t.t34 + p.W * t.t44;
		
		
		return newP;
	}


	//Apply transformation t to transformation oldT, and return the result.
	Transformation Transformer::compose(const Transformation& oldT, const Transformation& t) const
	{
		struct Transformation newT;
		
		//Top row
		newT.t11 = oldT.t11 * t.t11  +  oldT.t12 * t.t21  +  oldT.t13 * t.t31  +  oldT.t14 * t.t41;
		newT.t12 = oldT.t11 * t.t12  +  oldT.t12 * t.t22  +  oldT.t13 * t.t32  +  oldT.t14 * t.t42;
		newT.t13 = oldT.t11 * t.t13  +  oldT.t12 * t.t23  +  oldT.t13 * t.t33  +  oldT.t14 * t.t43;
		newT.t14 = oldT.t11 * t.t14  +  oldT.t12 * t.t24  +  oldT.t13 * t.t34  +  oldT.t14 * t.t44;

		newT.t21 = oldT.t21 * t.t11  +  oldT.t22 * t.t21  +  oldT.t23 * t.t31  +  oldT.t24 * t.t41;
		newT.t22 = oldT.t21 * t.t12  +  oldT.t22 * t.t22  +  oldT.t23 * t.t32  +  oldT.t24 * t.t42;
		newT.t23 = oldT.t21 * t.t13  +  oldT.t22 * t.t23  +  oldT.t23 * t.t33  +  oldT.t24 * t.t43;
		newT.t24 = oldT.t21 * t.t14  +  oldT.t22 * t.t24  +  oldT.t23 * t.t34  +  oldT.t24 * t.t44;

		newT.t31 = oldT.t31 * t.t11  +  oldT.t32 * t.t21  +  oldT.t33 * t.t31  +  oldT.t34 * t.t41;
		newT.t32 = oldT.t31 * t.t12  +  oldT.t32 * t.t22  +  oldT.t33 * t.t32  +  oldT.t34 * t.t42;
		newT.t33 = oldT.t31 * t.t13  +  oldT.t32 * t.t23  +  oldT.t33 * t.t33  +  oldT.t34 * t.t43;
		newT.t34 = oldT.t31 * t.t14  +  oldT.t32 * t.t24  +  oldT.t33 * t.t34  +  oldT.t34 * t.t44;

		newT.t41 = oldT.t41 * t.t11  +  oldT.t42 * t.t21  +  oldT.t43 * t.t31  +  oldT.t44 * t.t41;
		newT.t42 = oldT.t41 * t.t12  +  oldT.t42 * t.t22  +  oldT.t43 * t.t32  +  oldT.t44 * t.t42;
		newT.t43 = oldT.t41 * t.t13  +  oldT.t42 * t.t23  +  oldT.t43 * t.t33  +  oldT.t44 * t.t43;
		newT.t44 = oldT.t41 * t.t14  +  oldT.t42 * t.t24  +  oldT.t43 * t.t34  +  oldT.t44 * t.t44;
		
		return newT;
	}

	Transformation Transformer::translation(const double dx, const double dy, const double dz) const
	{
		struct Transformation t;
		
		t.t11 = 1.0;  t.t12 = 0.0;  t.t13 = 0.0;  t.t14 = 0.0;
		t.t21 = 0.0;  t.t22 = 1.0;  t.t23 = 0.0;  t.t24 = 0.0;
		t.t31 = 0.0;  t.t32 = 0.0;  t.t33 = 1.0;  t.t34 = 0.0;
		t.t41 = dx;   t.t42 = dy;   t.t43 = dz;   t.t44 = 1.0;
		
		return t;
	}

	
	//Rotate through angle theta about the x axis
	Transformation Transformer::xAxisRotation(const double theta) const
	{
		struct Transformation t;
		double cos = std::cos(theta);
		double sin = std::sin(theta);
		
		t.t11 = 1.0;  t.t12 = 0.0;  t.t13 = 0.0;  t.t14 = 0.0;
		t.t21 = 0.0;  t.t22 = cos;  t.t23 = sin;  t.t24 = 0.0;
		t.t31 = 0.0;  t.t32 = -sin; t.t33 = cos;  t.t34 = 0.0;
		t.t41 = 0.0;  t.t42 = 0.0;  t.t43 = 0.0;  t.t44 = 1.0;
		
		return t;
	}

	
	// Rotate through angle theta about the y axis
	Transformation Transformer::yAxisRotation(const double theta) const
	{
		struct Transformation t;
		double cos = std::cos(theta);
		double sin = std::sin(theta);
		
		t.t11 = cos;  t.t12 = 0.0;  t.t13 = -sin; t.t14 = 0.0;
		t.t21 = 0.0;  t.t22 = 1.0;  t.t23 = 0.0;  t.t24 = 0.0;
		t.t31 = sin;  t.t32 = 0.0;  t.t33 = cos;  t.t34 = 0.0;
		t.t41 = 0.0;  t.t42 = 0.0;  t.t43 = 0.0;  t.t44 = 1.0;
		
		return t;
	}


	// Rotate through angle theta about the z axis
	Transformation Transformer::zAxisRotation(const double theta) const
	{
		struct Transformation t;
		double cos = std::cos(theta);
		double sin = std::sin(theta);
		
		t.t11 = cos;  t.t12 = sin;  t.t13 = 0.0;  t.t14 = 0.0;
		t.t21 = -sin; t.t22 = cos;  t.t23 = 0.0;  t.t24 = 0.0;
		t.t31 = 0.0;  t.t32 = 0.0;  t.t33 = 1.0;  t.t34 = 0.0;
		t.t41 = 0.0;  t.t42 = 0.0;  t.t43 = 0.0;  t.t44 = 1.0;
		
		return t;
	}

	// Rotate through angle theta about unit vector u
	Transformation Transformer::unitVectorRotation(const double theta, const Vector& u) const
	{
		struct Transformation t;
		double cos = std::cos(theta);
		double sin = std::sin(theta);
		
		t.t11 = cos + u.x * u.x * (1 - cos);  
		t.t12 = u.x * u.y * (1 - cos) - u.z * sin;  
		t.t13 = u.x * u.z * (1 - cos) + u.y * sin; 
		t.t14 = 0.0;
		
		t.t21 = u.y * u.x * (1 - cos) + u.z * sin;  
		t.t22 = cos + u.y * u.y * (1 - cos);  
		t.t23 = u.y * u.z * (1 - cos) - u.x * sin;  
		t.t24 = 0.0;
		
		t.t31 = u.z * u.x * (1 - cos) - u.y * sin;  
		t.t32 = u.z * u.y * (1 - cos) + u.x * sin;  
		t.t33 = cos + u.z * u.z * (1 - cos);  
		t.t34 = 0.0;
		
		t.t41 = 0.0;  
		t.t42 = 0.0;  
		t.t43 = 0.0;  
		t.t44 = 1.0;
		
		return t;
	}


	Transformation Transformer::scaling(const double sx, const double sy, const double sz) const
	{
		struct Transformation t;
		
		t.t11 = sx;   t.t12 = 0.0;  t.t13 = 0.0;  t.t14 = 0.0;
		t.t21 = 0.0;  t.t22 = sy;   t.t23 = 0.0;  t.t24 = 0.0;
		t.t31 = 0.0;  t.t32 = 0.0;  t.t33 = sz;   t.t34 = 0.0;
		t.t41 = 0.0;  t.t42 = 0.0;  t.t43 = 0.0;  t.t44 = 1.0;
		
		return t;
	}

