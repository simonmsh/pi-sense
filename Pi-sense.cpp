#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <csignal>
#include <cmath>
//#include <iomanip>
#include "Vector.h"
#include "Transformation.h"
#include "WriteToWebserver.h"
#include "Transformer.h"
#include "VectorUtils.h"
#include "MatrixUtils.h"
#include "Matrix3d.h"
#include "IMUInterface.h"

/*
 * This class gathers the following data items from a Sense Hat and makes them available via a simple web service:
 * - Air pressure, rate of change of air pressure, confidence interval for rate of change of air pressure.
 * - Magnetic field magnitude, bearing and inclination.
 * - Humidity
 * - Temperature
 * - Gravity magnitude
 */

bool running = true;

void exitHandler(int s)
{
	std::cout << "Detected Ctrl-C signal number " << s <<  std::endl;
	running = false;
}

void counter(int id, int numIterations) 
{
	for (int i=0; i < numIterations; i++) 
	{
		std::cout << "Counter " << id << " has value " << i << std::endl;
	}
}

int main() {
	std::signal(SIGINT, exitHandler);
	std::string line;
    const std::string magFilename = "//var//www//html//mag.xml";

	//auto imuInterface = std::make_unique<IMUInterface>();
	//std::thread imuThread {&IMUInterface::process, &imuInterface};

	auto matrixUtils = std::make_unique<MatrixUtils>();
	auto vectorUtils = std::make_unique<VectorUtils>();
	auto transformer = std::make_unique<Transformer>();

	//struct Vector a;
	//a.x = 0.0; a.y = 1.0; a.z = 0.0; a.W = 1.0;
	//std::cout << "a=" << vectorUtils->toString(a) << std::endl;
	
	//struct Transformation identity = transformer->identity();
	//a = transformer->transform(a, identity);
	//std::cout << "a=" << vectorUtils->toString(a) << std::endl;
	
	//struct Transformation xAxisRot = transformer->xAxisRotation(1.0);
	//a = transformer->transform(a, xAxisRot);
	//std::cout << "a=" << vectorUtils->toString(a) << std::endl;

	//struct Transformation yAxisRot = transformer->yAxisRotation(1.0);
	//a = transformer->transform(a, yAxisRot);
	//std::cout << "a=" << vectorUtils->toString(a) << std::endl;

	//struct Transformation zAxisRot = transformer->zAxisRotation(1.0);
	//a = transformer->transform(a, zAxisRot);
	//std::cout << "a=" << vectorUtils->toString(a) << std::endl;


	//struct Vector zAxis;
	//zAxis.x = 0.0; zAxis.y = 0.0; zAxis.z = 1.0; zAxis.W = 1.0;
	//struct Transformation unitVectorRot = transformer->unitVectorRotation(3.1415926535/4.0, zAxis);
	//std::cout << "t=" << transformer->toString(unitVectorRot);
	//a = transformer->transform(a, unitVectorRot);
	//std::cout << "a=" << vectorUtils->toString(a) << std::endl;



    //std::vector<std::vector<double>> a(3,std::vector<double> (3,0.0)); 
    //a[0][0] = 1.0; a[0][1] = -2.2; a[0][2] = 3.0;
    //a[1][0] = 4.0; a[1][1] = 5.0; a[1][2] = 6.0;
    //a[2][0] = 7.0; a[2][1] = 8.0; a[2][2] = 6.0;
	//std::cout << "a=" << matrixUtils->toString(a) << std::endl;

    //std::vector<std::vector<double>> b(3,std::vector<double> (3,0.0)); 
    //b[0][0] = 1.0; b[0][1] = 2.0; b[0][2] = 3.0;
    //b[1][0] = 4.0; b[1][1] = 5.0; b[1][2] = 6.0;
    //b[2][0] = 7.0; b[2][1] = 8.0; b[2][2] = 9.0;
	//std::cout << matrixUtils->toString(b) << std::endl;

    //std::vector<std::vector<double>> unit(3,std::vector<double> (3,0.0)); 
    //unit[0][0] = 1.0; unit[0][1] = 0.0; unit[0][2] = 0.0;
    //unit[1][0] = 0.0; unit[1][1] = 1.0; unit[1][2] = 0.0;
    //unit[2][0] = 0.0; unit[2][1] = 0.0; unit[2][2] = 1.0;
	//std::cout << matrixUtils->toString(unit) << std::endl;

	//std::vector<std::vector<double>> l(3,std::vector<double> (3,0.0));
	//std::vector<std::vector<double>> u(3,std::vector<double> (3,0.0));
	//matrixUtils->LUDecomposition(a, l, u);	
	//std::cout << "l=" << matrixUtils->toString(l) << std::endl;
	//std::cout << "u=" << matrixUtils->toString(u) << std::endl;

	//std::vector<std::vector<double>> c(3,std::vector<double> (3,0.0));
	//c = matrixUtils->MatrixMultiply(l, u);	
	//std::cout << "c=" << matrixUtils->toString(c) << std::endl;
	//std::cout << "Det(c)=" << std::to_string(matrixUtils->LUDetermintant(l,u)) << std::endl;

	//std::vector<std::vector<double>> c(3,std::vector<double> (3,0.0));
	//std::vector<std::vector<double>> gaussC(3,std::vector<double> (3,0.0));
	//gaussC = matrixUtils->Gauss(c);
	//std::cout << "gaussC=" << std::endl;
	//std::cout << matrixUtils->toString(gaussC) << std::endl;

	//std::vector<std::vector<double>> d(3,std::vector<double> (4,0.0)); 
	//d[0][0] =  2.0; d[0][1] =  1.0; d[0][2] = -1.0; d[0][3] =   8.0;
	//d[1][0] = -3.0; d[1][1] = -1.0; d[1][2] =  2.0; d[1][3] = -11.0;
	//d[2][0] = -2.0; d[2][1] =  1.0; d[2][2] =  2.0; d[2][3] =  -3.0;
	//std::cout << matrixUtils->toString(d) << std::endl;
	//std::cout << matrixUtils->toString(matrixUtils->Gauss(d)) << std::endl;

	//std::vector<std::vector<double>> d(3,std::vector<double> (3,0.0)); 
	//d[0][0] =  5.0; d[0][1] =  7.0; d[0][2] = 9.0;
	//d[1][0] =  4.0; d[1][1] =  3.0; d[1][2] = 8.0;
	//d[2][0] =  7.0; d[2][1] =  5.0; d[2][2] = 6.0;
	//std::cout << "d=" << std::endl << matrixUtils->toString(d) << std::endl;
	//std::cout << "inverse(d)=" << std::endl << matrixUtils->toString(matrixUtils->Inverse(d)) << std::endl;
	//std::cout << "tranpose(d)=" << std::endl << matrixUtils->toString(matrixUtils->Transpose(d)) << std::endl;



	IMUInterface imuInterface;
	//auto imuInterface = std::make_unique<IMUInterface>();
	std::thread imuThread(&IMUInterface::init, &imuInterface);
	std::array <double, 100> accelMagnitudesSnapshot;
										
	auto webserverWriter = std::make_unique<WriteToWebserver>();

	while (running) {
		std::this_thread::sleep_for(std::chrono::milliseconds(10000));
		std::string buffer;
		std::time_t t = std::time(0);   // get time now
		std::tm* now = std::localtime(&t);
		struct Vector mag = imuInterface.getMag();
		std::pair <double, double> pressureGradient = imuInterface.getPressureGradient();

		buffer += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"; 
		buffer += "<data>\n";
		buffer.append("<magnetic>" + vectorUtils->toString(mag) + "</magnetic>\n"); 
		buffer.append("<accelerometer>" + vectorUtils->toString(imuInterface.getAccel()) + "</accelerometer>\n"); 
		buffer.append("<pressure>\n<current>" + std::to_string(imuInterface.getPressure()) + 
					"</current>\n<gradient><est>" + std::to_string(pressureGradient.first) + 
					"</est><conf95>" + 
					"[" + std::to_string(pressureGradient.first - pressureGradient.second) + ", " +
					std::to_string(pressureGradient.first + pressureGradient.second) + "]" +
					"</conf95></gradient>\n</pressure>\n"); 
		buffer.append("<temperature>" + std::to_string(imuInterface.getTemperature()) + "</temperature>\n"); 
		buffer.append("<humidity>" + std::to_string(imuInterface.getHumidity()) + "</humidity>\n"); 
		buffer.append("<bearing>" + std::to_string(vectorUtils->magneticBearing(mag)) + "</bearing>\n"); 
		buffer.append("<inclination>" + std::to_string(vectorUtils->magneticInclination(mag)) + "</inclination>\n"); 
		buffer.append("<accelStandardDeviation>" + std::to_string(imuInterface.getAccelGreatestDeviation()) + "</accelStandardDeviation>\n"); 
		buffer.append("<sampleFrequency>" + std::to_string(imuInterface.getSampleFrequency()) + "</sampleFrequency>\n"); 
		buffer.append("<time>" + std::to_string(now->tm_hour) + ':' + std::to_string(now->tm_min) + ':' +  std::to_string(now->tm_sec) + "</time>\n"); 

		buffer.append("<snapshot>"); 
		accelMagnitudesSnapshot = imuInterface.getAccelMagnitudesSnapshot();
		for (int i=0; i < 100 && accelMagnitudesSnapshot[i] > 0.0; i++) 
		  buffer.append("<sample>" + std::to_string(accelMagnitudesSnapshot[i]) + "</sample><deviation>" + 
		  std::to_string(1000.0 * std::abs(vectorUtils->magnitude(imuInterface.getAccel()) - accelMagnitudesSnapshot[i])) + "</deviation>\n");
		buffer.append("</snapshot>\n"); 
		
		buffer += "</data>";
		
		webserverWriter->writeString(buffer, magFilename);
		std::cout << std::endl << buffer << std::endl << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(90000));
	}
	imuInterface.terminate();
	imuThread.join();
	std::cout << "Exiting" << std::endl;
	return 0;
}

