#pragma once

struct Matrix3d
{
		double t11; double t12; double t13;
		double t21; double t22; double t23;
		double t31; double t32; double t33;
};
